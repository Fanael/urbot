// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot

import java.util

import net.dv8tion.jda.core.entities.Guild
import net.dv8tion.jda.core.entities.Role
import org.scalamock.scalatest.MockFactory
import org.scalatest.BeforeAndAfterAll
import org.scalatest.FlatSpec
import org.scalatest.Matchers

class RoleCategorizerTest extends FlatSpec with Matchers with BeforeAndAfterAll with MockFactory {
  override def beforeAll(): Unit = {
    Common.testDatabase.withStatement("DELETE FROM roles")(_.executeUpdate())
    Common.testDatabase.withStatement("DELETE FROM role_categories")(_.executeUpdate())
  }

  "Role categorizer" should "add new categories" in {
    val guildId = new DiscordId[Guild](1)
    categorizer.addCategory(guildId, "test 1", "abcd") shouldBe true
    categorizer.addCategory(guildId, "test 2", "efgh") shouldBe true
    categorizer.getRoleCategories(guildId) shouldBe Seq(("test 1", "abcd"), ("test 2", "efgh"))
  }

  it should "ignore addition of already existing categories" in {
    val guildId = new DiscordId[Guild](1)
    categorizer.addCategory(guildId, "test 1", "xyz") shouldBe false
    categorizer.getRoleCategories(guildId) shouldBe Seq(("test 1", "abcd"), ("test 2", "efgh"))
  }

  it should "update descriptions" in {
    val guildId = new DiscordId[Guild](1)
    categorizer.updateCategory(guildId, "test 1", "ijkl") shouldBe true
    categorizer.getRoleCategories(guildId) shouldBe Seq(("test 1", "ijkl"), ("test 2", "efgh"))
  }

  it should "ignore updates of nonexisting categories" in {
    val guildId = new DiscordId[Guild](1)
    categorizer.updateCategory(guildId, "test 1", "ijkl") shouldBe true
    categorizer.getRoleCategories(guildId) shouldBe Seq(("test 1", "ijkl"), ("test 2", "efgh"))
  }

  it should "remove categories" in {
    val guildId = new DiscordId[Guild](1)
    categorizer.removeCategory(guildId, "test 2") shouldBe true
    categorizer.getRoleCategories(guildId) shouldBe Seq(("test 1", "ijkl"))
  }

  it should "ignore removal of nonexisting categories" in {
    val guildId = new DiscordId[Guild](1)
    categorizer.removeCategory(guildId, "test 3") shouldBe false
    categorizer.getRoleCategories(guildId) shouldBe Seq(("test 1", "ijkl"))
  }

  it should "import a role sucessfully" in {
    val guild = mockGuild
    val role = mock[Role]
    val guildId = DiscordId(guild)
    (guild.getRolesByName _).expects(*, *).atLeastOnce().returns(util.Arrays.asList(role))
    (role.getName _: () => String).expects().atLeastOnce().returns("foo")
    (role.getIdLong _: () => Long).expects().atLeastOnce().returns(ROLE_ID)
    categorizer.importRole(guild, "test 1", "foo") shouldBe ImportResult.Ok
    categorizer.getRolesInCategory(guildId, "test 1") shouldBe Some(Seq("foo"))
  }

  it should "not import a role into a nonexisting category" in {
    val guild = mockGuild
    val role = mock[Role]
    val guildId = DiscordId(guild)
    (guild.getRolesByName _).expects(*, *).anyNumberOfTimes().returns(util.Arrays.asList(role))
    categorizer.importRole(guild, "test 2", "foo") shouldBe ImportResult.CategoryNotFound
    categorizer.getRolesInCategory(guildId, "test 1") shouldBe Some(Seq("foo"))
    categorizer.getRolesInCategory(guildId, "test 2") shouldBe None
  }

  it should "not import ambiguous role names" in {
    val guild = mockGuild
    val role1 = mock[Role]
    val role2 = mock[Role]
    val guildId = DiscordId(guild)
    (guild.getRolesByName _).expects(*, *).anyNumberOfTimes().returns(util.Arrays.asList(role1, role2))
    categorizer.importRole(guild, "test 1", "bar") shouldBe ImportResult.MultipleRolesFound
    categorizer.getRolesInCategory(guildId, "test 1") shouldBe Some(Seq("foo"))
  }

  it should "forget roles successfully" in {
    val guildId = new DiscordId[Guild](1)
    categorizer.forgetRole(guildId, "test 1", "foo") shouldBe true
    categorizer.getRolesInCategory(guildId, "test 1") shouldBe Some(Seq())
  }

  it should "not forget nonexisting role" in {
    val guildId = new DiscordId[Guild](1)
    categorizer.forgetRole(guildId, "test 1", "bar") shouldBe false
    categorizer.getRolesInCategory(guildId, "test 1") shouldBe Some(Seq())
  }

  it should "rename a role successfully" in {
    val guild = mockGuild
    val role = mock[Role]
    val guildId = DiscordId(guild)
    (role.getIdLong _: () => Long).expects().anyNumberOfTimes().returns(ROLE_ID)
    (role.getName _: () => String).expects().anyNumberOfTimes().returns("bar")
    (guild.getRolesByName _).expects(*, *).anyNumberOfTimes().returns(util.Arrays.asList(role))
    categorizer.importRole(guild, "test 1", "foo")
    categorizer.renameRole(role)
    categorizer.getRolesInCategory(guildId, "test 1") shouldBe Some(Seq("bar"))
  }

  it should "find role ID by name in category" in {
    val guildId = new DiscordId[Guild](1)
    categorizer.findRoleIdInCategory(guildId, "test 1", "bar") shouldBe Some(new DiscordId(ROLE_ID))
  }

  it should "not find a role by name in category if it doesn't exist" in {
    val guildId = new DiscordId[Guild](1)
    categorizer.findRoleIdInCategory(guildId, "test 2", "bar") shouldBe None
    categorizer.findRoleIdInCategory(guildId, "test 1", "foo") shouldBe None
  }

  it should "find an unambiguous role by name in guild" in {
    val guildId = new DiscordId[Guild](1)
    categorizer.findRoleIdInGuild(guildId, "bar") shouldBe Seq(new DiscordId(ROLE_ID))
  }

  it should "find all matching roles by name in guild if name's ambiguous" in {
    val guild = mockGuild
    val role = mock[Role]
    val guildId = DiscordId(guild)
    (role.getIdLong _: () => Long).expects().anyNumberOfTimes().returns(5)
    (role.getName _: () => String).expects().anyNumberOfTimes().returns("bar")
    (guild.getRolesByName _).expects(*, *).anyNumberOfTimes().returns(util.Arrays.asList(role))
    categorizer.addCategory(guildId, "test 2", "")
    categorizer.importRole(guild, "test 2", "bar")
    categorizer.findRoleIdInGuild(guildId, "bar") shouldBe Seq(new DiscordId(ROLE_ID), new DiscordId(5))
  }

  it should "find a role if it's in multiple categories" in {
    val guild = mockGuild
    val role = mock[Role]
    (role.getIdLong _: () => Long).expects().anyNumberOfTimes().returns(5)
    (role.getName _: () => String).expects().anyNumberOfTimes().returns("qux")
    (guild.getRolesByName _).expects(*, *).anyNumberOfTimes().returns(util.Arrays.asList(role))
    categorizer.addCategory(DiscordId(guild), "test 3", "")
    categorizer.importRole(guild, "test 2", role.getName)
    categorizer.importRole(guild, "test 3", role.getName)
    categorizer.findRoleIdInGuild(DiscordId(guild), role.getName) shouldBe Seq(new DiscordId(5))
  }

  private def mockGuild: Guild = {
    val guild = mock[Guild]
    (guild.getIdLong _: () => Long).expects().anyNumberOfTimes().returns(1)
    guild
  }

  private val ROLE_ID: Long = 1
  private val categorizer = new RoleCategorizer(Common.testDatabase)
}
