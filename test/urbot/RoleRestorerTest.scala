// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot

import scala.collection.mutable

import net.dv8tion.jda.core.entities.Guild
import net.dv8tion.jda.core.entities.Member
import net.dv8tion.jda.core.entities.Role
import net.dv8tion.jda.core.entities.User
import org.scalamock.scalatest.MockFactory
import org.scalatest.BeforeAndAfterEach
import org.scalatest.FlatSpec
import org.scalatest.Matchers
import urbot.JDBCExtensions.BindableStatement

class RoleRestorerTest extends FlatSpec with Matchers with BeforeAndAfterEach with MockFactory {
  override def beforeEach(): Unit = {
    Common.testDatabase.withStatement("DELETE FROM persisted_nicknames")(_.executeUpdate())
    Common.testDatabase.withStatement("DELETE FROM persisted_user_roles")(_.executeUpdate())
    Common.testDatabase.withStatement("INSERT INTO persisted_user_roles VALUES (?, ?, ?)") { insertRoleStmt =>
      insertRoleStmt.bind(FIRST_GUILD_ID).bind(FIRST_USER_ID).bind(FIRST_ROLE_ID)
      insertRoleStmt.executeUpdate()
      insertRoleStmt.bind(FIRST_GUILD_ID).bind(FIRST_USER_ID).bind(SECOND_ROLE_ID)
      insertRoleStmt.executeUpdate()
      insertRoleStmt.bind(FIRST_GUILD_ID).bind(FIRST_USER_ID).bind(THIRD_ROLE_ID)
      insertRoleStmt.executeUpdate()
      insertRoleStmt.bind(FIRST_GUILD_ID).bind(SECOND_USER_ID).bind(SECOND_ROLE_ID)
      insertRoleStmt.executeUpdate()
    }
    Common.testDatabase.withStatement("INSERT INTO persisted_nicknames VALUES (?, ?, ?)") { insertNicknameStmt =>
      insertNicknameStmt.bind(FIRST_GUILD_ID).bind(FIRST_USER_ID).bind("foo")
      insertNicknameStmt.executeUpdate()
      insertNicknameStmt.bind(FIRST_GUILD_ID).bind(THIRD_USER_ID).bind("qux")
      insertNicknameStmt.executeUpdate()
    }
  }

  "Role restorer" should "restore user roles and nickname" in {
    val member = mock[Member]
    val guild = mock[Guild]
    val user = mock[User]
    val role = mock[Role]

    (member.getGuild _: () => Guild).expects().anyNumberOfTimes().returns(guild)
    (member.getUser _: () => User).expects().anyNumberOfTimes().returns(user)
    (guild.getIdLong _: () => Long).expects().anyNumberOfTimes().returns(FIRST_GUILD_ID.value)
    (user.getIdLong _: () => Long).expects().anyNumberOfTimes().returns(FIRST_USER_ID.value)
    (guild.getRoleById(_: Long)).expects(FIRST_ROLE_ID.value).returns(role)
    (guild.getRoleById(_: Long)).expects(SECOND_ROLE_ID.value).returns(role)
    (guild.getRoleById(_: Long)).expects(THIRD_ROLE_ID.value).returns(role)

    roleRestorer.getRestoredRolesAndNickname(member) shouldBe ((mutable.Buffer(role, role, role), Some("foo")))
  }

  it should "restore only roles when no nickname found" in {
    val member = mock[Member]
    val guild = mock[Guild]
    val user = mock[User]
    val role = mock[Role]

    (member.getGuild _: () => Guild).expects().anyNumberOfTimes().returns(guild)
    (member.getUser _: () => User).expects().anyNumberOfTimes().returns(user)
    (guild.getIdLong _: () => Long).expects().anyNumberOfTimes().returns(FIRST_GUILD_ID.value)
    (user.getIdLong _: () => Long).expects().anyNumberOfTimes().returns(SECOND_USER_ID.value)
    (guild.getRoleById(_: Long)).expects(SECOND_ROLE_ID.value).returns(role)

    roleRestorer.getRestoredRolesAndNickname(member) shouldBe ((mutable.Buffer(role), None))
  }

  it should "restore only nickname when no roles found" in {
    val member = mock[Member]
    val guild = mock[Guild]
    val user = mock[User]

    (member.getGuild _: () => Guild).expects().anyNumberOfTimes().returns(guild)
    (member.getUser _: () => User).expects().anyNumberOfTimes().returns(user)
    (guild.getIdLong _: () => Long).expects().anyNumberOfTimes().returns(FIRST_GUILD_ID.value)
    (user.getIdLong _: () => Long).expects().anyNumberOfTimes().returns(THIRD_USER_ID.value)

    roleRestorer.getRestoredRolesAndNickname(member) shouldBe ((mutable.Buffer(), Some("qux")))
  }

  it should "restore nothing if a new user is found" in {
    val member = mock[Member]
    val guild = mock[Guild]
    val user = mock[User]

    (member.getGuild _: () => Guild).expects().anyNumberOfTimes().returns(guild)
    (member.getUser _: () => User).expects().anyNumberOfTimes().returns(user)
    (guild.getIdLong _: () => Long).expects().anyNumberOfTimes().returns(SECOND_GUILD_ID.value)
    (user.getIdLong _: () => Long).expects().anyNumberOfTimes().returns(FIRST_USER_ID.value)

    roleRestorer.getRestoredRolesAndNickname(member) shouldBe ((mutable.Buffer(), None))
  }

  it should "forget user when requested" in {
    roleRestorer.removeUser(FIRST_GUILD_ID, FIRST_USER_ID)

    val member = mock[Member]
    val guild = mock[Guild]
    val user = mock[User]

    (member.getGuild _: () => Guild).expects().anyNumberOfTimes().returns(guild)
    (member.getUser _: () => User).expects().anyNumberOfTimes().returns(user)
    (guild.getIdLong _: () => Long).expects().anyNumberOfTimes().returns(FIRST_GUILD_ID.value)
    (user.getIdLong _: () => Long).expects().anyNumberOfTimes().returns(FIRST_USER_ID.value)

    roleRestorer.getRestoredRolesAndNickname(member) shouldBe ((mutable.Buffer(), None))
  }

  it should "add roles to user" in {
    val member = mock[Member]
    val guild = mock[Guild]
    val user = mock[User]
    val role = mock[Role]

    (member.getGuild _: () => Guild).expects().anyNumberOfTimes().returns(guild)
    (member.getUser _: () => User).expects().anyNumberOfTimes().returns(user)
    (guild.getIdLong _: () => Long).expects().anyNumberOfTimes().returns(SECOND_GUILD_ID.value)
    (user.getIdLong _: () => Long).expects().anyNumberOfTimes().returns(FIRST_USER_ID.value)
    (role.getIdLong _: () => Long).expects().anyNumberOfTimes().returns(FIRST_ROLE_ID.value)
    (guild.getRoleById(_: Long)).expects(role.getIdLong).once().returns(role)

    roleRestorer.addUserRoles(member, Seq(role))
    roleRestorer.getRestoredRolesAndNickname(member) shouldBe ((mutable.Buffer(role), None))
  }

  it should "remove roles from user" in {
    val member = mock[Member]
    val guild = mock[Guild]
    val user = mock[User]
    val role = mock[Role]

    (member.getGuild _: () => Guild).expects().anyNumberOfTimes().returns(guild)
    (member.getUser _: () => User).expects().anyNumberOfTimes().returns(user)
    (guild.getIdLong _: () => Long).expects().anyNumberOfTimes().returns(FIRST_GUILD_ID.value)
    (user.getIdLong _: () => Long).expects().anyNumberOfTimes().returns(FIRST_USER_ID.value)
    (role.getIdLong _: () => Long).expects().anyNumberOfTimes().returns(THIRD_ROLE_ID.value)
    (guild.getRoleById(_: Long)).expects(FIRST_ROLE_ID.value).returns(role)
    (guild.getRoleById(_: Long)).expects(SECOND_ROLE_ID.value).returns(role)

    roleRestorer.removeUserRoles(member, Seq(role))
    roleRestorer.getRestoredRolesAndNickname(member) shouldBe ((mutable.Buffer(role, role), Some("foo")))
  }

  it should "remove roles from guild" in {
    val member = mock[Member]
    val guild = mock[Guild]
    val user = mock[User]
    val role = mock[Role]
    val member2 = mock[Member]
    val user2 = mock[User]

    (member.getGuild _: () => Guild).expects().anyNumberOfTimes().returns(guild)
    (member.getUser _: () => User).expects().anyNumberOfTimes().returns(user)
    (guild.getIdLong _: () => Long).expects().anyNumberOfTimes().returns(FIRST_GUILD_ID.value)
    (user.getIdLong _: () => Long).expects().anyNumberOfTimes().returns(FIRST_USER_ID.value)
    (role.getIdLong _: () => Long).expects().anyNumberOfTimes().returns(SECOND_ROLE_ID.value)
    (member2.getGuild _: () => Guild).expects().anyNumberOfTimes().returns(guild)
    (member2.getUser _: () => User).expects().anyNumberOfTimes().returns(user2)
    (user2.getIdLong _: () => Long).expects().anyNumberOfTimes().returns(SECOND_USER_ID.value)
    (guild.getRoleById(_: Long)).expects(FIRST_ROLE_ID.value).returns(role)
    (guild.getRoleById(_: Long)).expects(THIRD_ROLE_ID.value).returns(role)

    roleRestorer.forgetRole(FIRST_GUILD_ID, SECOND_ROLE_ID)
    roleRestorer.getRestoredRolesAndNickname(member) shouldBe ((mutable.Buffer(role, role), Some("foo")))
    roleRestorer.getRestoredRolesAndNickname(member2) shouldBe ((mutable.Buffer(), None))
  }

  it should "rename user" in {
    val member = mock[Member]
    val guild = mock[Guild]
    val user = mock[User]

    (member.getGuild _: () => Guild).expects().anyNumberOfTimes().returns(guild)
    (member.getUser _: () => User).expects().anyNumberOfTimes().returns(user)
    (guild.getIdLong _: () => Long).expects().anyNumberOfTimes().returns(FIRST_GUILD_ID.value)
    (user.getIdLong _: () => Long).expects().anyNumberOfTimes().returns(THIRD_USER_ID.value)

    roleRestorer.changeNickname(member, Some("bar"))
    roleRestorer.getRestoredRolesAndNickname(member) shouldBe ((mutable.Buffer(), Some("bar")))
  }

  it should "remove user nickname when changing to None" in {
    val member = mock[Member]
    val guild = mock[Guild]
    val user = mock[User]

    (member.getGuild _: () => Guild).expects().anyNumberOfTimes().returns(guild)
    (member.getUser _: () => User).expects().anyNumberOfTimes().returns(user)
    (guild.getIdLong _: () => Long).expects().anyNumberOfTimes().returns(FIRST_GUILD_ID.value)
    (user.getIdLong _: () => Long).expects().anyNumberOfTimes().returns(THIRD_USER_ID.value)

    roleRestorer.changeNickname(member, None)
    roleRestorer.getRestoredRolesAndNickname(member) shouldBe ((mutable.Buffer(), None))
  }

  private final val FIRST_GUILD_ID = new DiscordId[Guild](1)
  private final val SECOND_GUILD_ID = new DiscordId[Guild](2)
  private final val FIRST_USER_ID = new DiscordId[User](1)
  private final val SECOND_USER_ID = new DiscordId[User](2)
  private final val THIRD_USER_ID = new DiscordId[User](3)
  private final val FIRST_ROLE_ID = new DiscordId[Role](1)
  private final val SECOND_ROLE_ID = new DiscordId[Role](2)
  private final val THIRD_ROLE_ID = new DiscordId[Role](3)
  private val roleRestorer = new RoleRestorer(Common.testDatabase)
}
