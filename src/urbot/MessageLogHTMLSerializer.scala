// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot

import java.io.StringWriter
import java.time.Instant
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter

import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.transform.OutputKeys
import javax.xml.transform.TransformerFactory
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamResult
import net.dv8tion.jda.core.entities.Message
import org.w3c.dom.Document
import org.w3c.dom.Element

private class MessageLogHTMLSerializer {
  def serializeEvent(event: MessageLog.Event): Unit = {
    if (currentMessageId.contains(event.messageId)) {
      addCurrentMessageEvent(event)
    } else {
      createNewMessage(event)
    }
  }

  def getHTML: String = {
    val transformer = TransformerFactory.newInstance().newTransformer()
    transformer.setOutputProperty(OutputKeys.METHOD, "xml")
    transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes")
    transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, "about:legacy-compat")
    transformer.setOutputProperty(OutputKeys.INDENT, "no")
    val writer = new StringWriter
    transformer.transform(new DOMSource(document), new StreamResult(writer))
    writer.toString
  }

  private def createNewMessage(event: MessageLog.Event): Unit = {
    currentMessageId = Some(event.messageId)
    val element = document.createElement("li")
    element.setAttribute("id", "msg" + event.messageId.value.toString)
    eventList.appendChild(element)
    currentMessageEntry = Some(element)
    serializeId(element, event.messageId)
    addCurrentMessageEvent(event)
  }

  private def serializeId(parent: Element, id: DiscordId[Message]): Unit = {
    val idString = id.value.toString
    val span = document.createElement("a")
    span.setTextContent(idString)
    span.setAttribute("class", "messageId")
    span.setAttribute("href", "#msg" + idString)
    parent.appendChild(span)
  }

  private def addCurrentMessageEvent(event: MessageLog.Event): Unit = {
    val eventElement = document.createElement("div")
    eventElement.setAttribute("class", "message " + (event.eventType match {
      case MessageLog.EventType.Create => "created"
      case MessageLog.EventType.Update => "updated"
      case MessageLog.EventType.Delete => "deleted"
    }))
    serializeTimestamp(eventElement, event.timestamp)
    event.content.foreach { content => serializeContent(eventElement, content) }
    currentMessageEntry.get.appendChild(eventElement)
  }

  private def serializeTimestamp(parent: Element, instant: Instant): Unit = {
    val offset = instant.atOffset(ZoneOffset.UTC)
    val time = document.createElement("time")
    time.setTextContent(offset.format(DateTimeFormatter.RFC_1123_DATE_TIME))
    time.setAttribute("datetime", offset.format(DateTimeFormatter.ISO_DATE_TIME))
    parent.appendChild(time)
  }

  private def serializeContent(parent: Element, content: String): Unit = {
    val span = document.createElement("span")
    span.setTextContent(content)
    parent.appendChild(span)
  }

  private var currentMessageId: Option[DiscordId[Message]] = None
  private var currentMessageEntry: Option[Element] = None
  private val document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument()
  private val eventList = MessageLogHTMLSerializer.createBoilerplate(document)
}

private[this] object MessageLogHTMLSerializer {
  private def createBoilerplate(document: Document): Element = {
    val htmlElement = document.createElement("html")
    document.appendChild(htmlElement)
    createHead(document, htmlElement)
    createBody(document, htmlElement)
  }

  private def createHead(document: Document, root: Element): Unit = {
    val head = document.createElement("head")
    root.appendChild(head)
    val metaCharset = document.createElement("meta")
    metaCharset.setAttribute("charset", "UTF-8")
    head.appendChild(metaCharset)
    val title = document.createElement("title")
    title.setTextContent("Message log")
    head.appendChild(title)
    val style = document.createElement("style")
    style.setTextContent(css)
    head.appendChild(style)
  }

  private def createBody(document: Document, root: Element): Element = {
    val body = document.createElement("body")
    root.appendChild(body)
    val ul = document.createElement("ul")
    body.appendChild(ul)
    ul.setAttribute("id", "messages")
    ul
  }

  private val css =
    """
body {
  margin: 0 auto;
  width: 100em;
  min-width: 50%;
  max-width: 100%;
  font-family: sans-serif;
  color: #222;
  background-color: #eee;
}

#messages {
  list-style-type: none;
  border: 1px solid #444;
  margin: 0;
  padding: 1em;
}

#messages li {
  padding: 0.1em 0;
  border-bottom: 1px dashed #444;
}

#messages li div {
  padding: 0.2em 0;
  white-space: pre-wrap;
}

.messageId {
  font-size: x-small;
  display: block;
  color: #666;
}

.updated {
  background-color: #cce;
}

.deleted {
  background-color: #ecc;
}

time { 
  font-size: small;
  display: block;
  color: #666;
}

.deleted time {
  font-size: larger;
}

.deleted time::before {
  content: "Deleted at: ";
}"""
}
