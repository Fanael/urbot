// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot

import net.dv8tion.jda.core.entities.Guild
import urbot.JDBCExtensions.BindableStatement

private abstract class PersistedModule protected(
  protected val databaseConnection: DatabaseConnection,
  val name: String) extends Module {

  final def isEnabledIn(guildId: DiscordId[Guild]): Boolean = {
    databaseConnection.withStatement("SELECT 1 FROM disabled_modules WHERE guild_id=? AND module_name=?") { stmt =>
      stmt.bind(guildId).bind(name)
      Utils.withResource(stmt.executeQuery())(!_.next())
    }
  }

  def enableIn(guild: Guild): Boolean = {
    databaseConnection.withStatement("DELETE FROM disabled_modules WHERE guild_id=? AND module_name=?") { stmt =>
      stmt.bind(DiscordId(guild)).bind(name)
      stmt.executeUpdate() > 0
    }
  }

  def disableIn(guild: Guild): Boolean = {
    databaseConnection.withStatement(
      "INSERT OR IGNORE INTO disabled_modules(guild_id, module_name) VALUES (?, ?)") { stmt =>
      stmt.bind(DiscordId(guild)).bind(name)
      stmt.executeUpdate() > 0
    }
  }
}
