// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot

import java.sql.ResultSet

import scala.collection.immutable.Vector
import scala.util.Try

import com.typesafe.scalalogging.Logger
import net.dv8tion.jda.core.entities.Guild
import net.dv8tion.jda.core.entities.Role
import urbot.JDBCExtensions.BindableStatement
import urbot.JDBCExtensions.ExtendedResultSet

private class RoleCategorizer(connection: DatabaseConnection) extends PersistedModule(connection, "role-categorizer") {

  import ImportResult.ImportResult

  def addCategory(guildId: DiscordId[Guild], categoryName: String, description: String): Boolean = {
    logger.info("Adding category `{}` in guild {}`", categoryName, guildId)
    databaseConnection.withStatement(
      "INSERT OR IGNORE INTO role_categories(guild_id, category_name, description) VALUES(?, ?, ?)") { stmt =>
      stmt.bind(guildId).bind(categoryName).bind(description)
      stmt.executeUpdate() > 0
    }
  }

  def updateCategory(guildId: DiscordId[Guild], categoryName: String, newDescription: String): Boolean = {
    logger.info("Updating category description `{}` in guild {}`", categoryName, guildId)
    databaseConnection.withStatement(
      "UPDATE role_categories SET description=? WHERE guild_id=? AND category_name=?") { stmt =>
      stmt.bind(newDescription).bind(guildId).bind(categoryName)
      stmt.executeUpdate() > 0
    }
  }

  def removeCategory(guildId: DiscordId[Guild], categoryName: String): Boolean = {
    logger.info("Removing category `{}` from guild {}`", categoryName, guildId)
    databaseConnection.withStatement("DELETE FROM role_categories WHERE guild_id=? AND category_name=?") { stmt =>
      stmt.bind(guildId).bind(categoryName)
      stmt.executeUpdate() > 0
    }
  }

  def importRole(guild: Guild, categoryName: String, roleName: String): ImportResult = {
    val guildId = DiscordId(guild)
    logger.info("Importing role `{}` into category `{}` in guild {}", roleName, categoryName, guildId)
    val roles = guild.getRolesByName(roleName, /* ignoreCase = */ true)
    roles.size match {
      case 0 => ImportResult.RoleNotFound
      case 1 => {
        findCategoryIdByName(guildId, categoryName) match {
          case None => ImportResult.CategoryNotFound
          case Some(categoryId) => {
            insertRole(categoryId, roles.get(0))
            ImportResult.Ok
          }
        }
      }
      case _ => ImportResult.MultipleRolesFound
    }
  }

  def forgetRole(guildId: DiscordId[Guild], categoryName: String, roleName: String): Boolean = {
    findCategoryIdByName(guildId, categoryName).exists { categoryId =>
      logger.info("Forgetting role `{}` in category ID {}", roleName, categoryId)
      databaseConnection.withStatement("DELETE FROM roles WHERE category_id=? AND role_name=?") { stmt =>
        stmt.bind(categoryId).bind(roleName)
        stmt.executeUpdate() > 0
      }
    }
  }

  def forgetRole(guildId: DiscordId[Guild], roleId: DiscordId[Role]): Unit = {
    logger.info("Forgetting role {} in guild {}", roleId, guildId)
    databaseConnection.withStatement(
      """DELETE FROM roles
WHERE category_id IN (SELECT category_id FROM role_categories WHERE guild_id=?) AND role_id=?""") { stmt =>
      stmt.bind(guildId).bind(roleId)
      stmt.executeUpdate()
    }
  }

  def pruneOldRoles(guild: Guild, tempTable: GuildRolesTemporaryTable): Unit = {
    val guildId = DiscordId(guild)
    val deletedRows = databaseConnection.withStatement(
      s"""DELETE FROM roles
WHERE category_id IN (SELECT category_id FROM role_categories WHERE guild_id=?)
  AND role_id NOT IN (SELECT role_id FROM ${tempTable.name})""") { stmt =>
      stmt.bind(guildId)
      stmt.executeUpdate()
    }
    logger.info("Pruned {} old roles from guild {}", deletedRows, guildId)
  }

  def addRole(guild: Guild, categoryName: String, roleName: String)(continuation: Try[Unit] => Unit): Unit = {
    val guildId = DiscordId(guild)
    findCategoryIdByName(guildId, categoryName).map { categoryId =>
      logger.info("Creating role `{}` in category ID {}", roleName, categoryId)
      val controller = guild.getController
      val action = controller.createRole()
      action.setName(roleName).submit().thenAccept { role =>
        continuation(Try(insertRole(categoryId, role)))
      }
    }
  }

  def getRoleCategories(guildId: DiscordId[Guild]): Seq[(String, String)] = {
    def getRow(rs: ResultSet): (String, String) = {
      val name = rs.getString(1)
      val description = rs.getString(2)
      (name, description)
    }

    logger.debug("Listing role categories in guild {}", guildId)
    databaseConnection.withStatement(
      "SELECT category_name, description FROM role_categories WHERE guild_id=? ORDER BY category_name") { stmt =>
      stmt.bind(guildId)
      Utils.withResource(stmt.executeQuery())(_.iterate(getRow).toVector)
    }
  }

  def getRolesInCategory(guildId: DiscordId[Guild], categoryName: String): Option[Seq[String]] = {
    logger.debug("Listing roles in category `{}` in guild {}", categoryName, guildId)
    findCategoryIdByName(guildId, categoryName).map { categoryId =>
      databaseConnection.withStatement("SELECT role_name FROM roles WHERE category_id=? ORDER BY role_name") { stmt =>
        stmt.bind(categoryId)
        Utils.withResource(stmt.executeQuery())(_.iterate(_.getString(1)).toVector)
      }
    }
  }

  def findRoleIdInCategory(guildId: DiscordId[Guild], categoryName: String,
    roleName: String): Option[DiscordId[Role]] = {
    logger.debug("Finding role {} in {}/{}", roleName, guildId, categoryName)
    findCategoryIdByName(guildId, categoryName).flatMap { categoryId =>
      databaseConnection.withStatement("SELECT role_id FROM roles WHERE category_id=? AND role_name=?") { stmt =>
        stmt.bind(categoryId).bind(roleName)
        Utils.withResource(stmt.executeQuery())(_.mapToOption(_.getId[Role](1)))
      }
    }
  }

  def findRoleIdInGuild(guildId: DiscordId[Guild], roleName: String): Vector[DiscordId[Role]] = {
    logger.debug("Finding role {} categoryless in {}", roleName, guildId)
    databaseConnection.withStatement(
      "SELECT role_id FROM roles JOIN role_categories USING (category_id) WHERE guild_id=? AND role_name=?") { stmt =>
      stmt.bind(guildId).bind(roleName)
      Utils.withResource(stmt.executeQuery())(_.iterate(_.getId[Role](1)).toVector.distinct)
    }
  }

  def renameRole(role: Role): Unit = {
    val roleId = DiscordId(role)
    val newName = role.getName
    logger.info("Renaming role ID {} to {}", roleId, newName)
    databaseConnection.withStatement("UPDATE roles SET role_name=? WHERE role_id=?") { stmt =>
      stmt.bind(newName).bind(roleId)
      stmt.executeUpdate()
    }
  }

  private def findCategoryIdByName(guildId: DiscordId[Guild], categoryName: String): Option[Long] = {
    databaseConnection.withStatement(
      "SELECT category_id FROM role_categories WHERE guild_id=? AND category_name=?") { stmt =>
      stmt.bind(guildId).bind(categoryName)
      Utils.withResource(stmt.executeQuery())(_.mapToOption(_.getLong(1)))
    }
  }

  private def insertRole(categoryId: Long, role: Role): Unit = {
    databaseConnection.withStatement("INSERT INTO roles(category_id, role_name, role_id) VALUES(?, ?, ?)") { stmt =>
      stmt.bind(categoryId).bind(role.getName).bind(DiscordId(role))
      stmt.executeUpdate()
    }
  }

  private val logger = Logger[RoleCategorizer]
}

private object ImportResult extends Enumeration {
  type ImportResult = Value

  val Ok, RoleNotFound, MultipleRolesFound, CategoryNotFound = Value
}
