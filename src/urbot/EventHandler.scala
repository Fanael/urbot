// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot

import scala.collection.JavaConverters.asScalaBufferConverter
import scala.collection.JavaConverters.bufferAsJavaListConverter

import net.dv8tion.jda.core.JDA
import net.dv8tion.jda.core.entities.Game
import net.dv8tion.jda.core.entities.Guild
import net.dv8tion.jda.core.entities.Member
import net.dv8tion.jda.core.events.Event
import net.dv8tion.jda.core.events.ReadyEvent
import net.dv8tion.jda.core.events.guild.GuildBanEvent
import net.dv8tion.jda.core.events.guild.member.GuildMemberJoinEvent
import net.dv8tion.jda.core.events.guild.member.GuildMemberLeaveEvent
import net.dv8tion.jda.core.events.guild.member.GuildMemberNickChangeEvent
import net.dv8tion.jda.core.events.guild.member.GuildMemberRoleAddEvent
import net.dv8tion.jda.core.events.guild.member.GuildMemberRoleRemoveEvent
import net.dv8tion.jda.core.events.message.MessageBulkDeleteEvent
import net.dv8tion.jda.core.events.message.MessageDeleteEvent
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import net.dv8tion.jda.core.events.message.MessageUpdateEvent
import net.dv8tion.jda.core.events.role.RoleDeleteEvent
import net.dv8tion.jda.core.events.role.update.RoleUpdateNameEvent
import net.dv8tion.jda.core.hooks.EventListener
import net.dv8tion.jda.core.requests.RestAction
import urbot.settings.ArrivalChannel
import urbot.settings.ArrivalMessage
import urbot.settings.Autorole
import urbot.settings.GuildSetting
import urbot.settings.LeaveMessage
import urbot.settings.WelcomeBackMessage

private class EventHandler(bot: Bot) extends EventListener {
  override def onEvent(event: Event): Unit = {
    event match {
      case _: ReadyEvent => ready(event.getJDA)
      case msgEvent: MessageReceivedEvent => messageReceived(msgEvent)
      case msgEvent: MessageUpdateEvent => messageUpdated(msgEvent)
      case msgEvent: MessageDeleteEvent => messageDeleted(msgEvent)
      case msgEvent: MessageBulkDeleteEvent => messagesDeleted(msgEvent)
      case banEvent: GuildBanEvent => ban(banEvent)
      case joinEvent: GuildMemberJoinEvent => join(joinEvent)
      case leaveEvent: GuildMemberLeaveEvent => leave(leaveEvent)
      case roleAddEvent: GuildMemberRoleAddEvent => memberRolesAdded(roleAddEvent)
      case roleDelEvent: GuildMemberRoleRemoveEvent => memberRolesRemoved(roleDelEvent)
      case nickChangeEvent: GuildMemberNickChangeEvent => nickChanged(nickChangeEvent)
      case roleDelEvent: RoleDeleteEvent => roleDeleted(roleDelEvent)
      case roleRenameEvent: RoleUpdateNameEvent => roleRenamed(roleRenameEvent)
      case _ => // We're not interested in this event.
    }
  }

  private def ready(jda: JDA): Unit = {
    setPresence(jda)
    jda.getGuilds.asScala.view.foreach { guild =>
      Utils.withResource(new GuildRolesTemporaryTable(bot.databaseConnection, guild)) { rolesTable =>
        val guildId = DiscordId(guild)
        if (bot.roleRestorer.isEnabledIn(guildId)) {
          bot.roleRestorer.snapshot(guild, rolesTable)
        }
        if (bot.roleCategorizer.isEnabledIn(guildId)) {
          bot.roleCategorizer.pruneOldRoles(guild, rolesTable)
        }
      }
    }
    bot.databaseConnection.startPeriodicCommit()
  }

  private def ban(event: GuildBanEvent): Unit = {
    val guildId = DiscordId(event.getGuild)
    if (bot.roleRestorer.isEnabledIn(guildId)) {
      bot.roleRestorer.removeUser(guildId, DiscordId(event.getUser))
    }
  }

  private def join(event: GuildMemberJoinEvent): Unit = {
    if (event.getUser.isBot) {
      // Don't bother with welcome messages when it's just a bot.
      return
    }
    val guild = event.getGuild
    if (!bot.roleRestorer.isEnabledIn(DiscordId(guild))) {
      return
    }
    val member = event.getMember
    bot.roleRestorer.getRestoredRolesAndNickname(member) match {
      case (Seq(), None) =>
        addAutorole(guild, member).foreach(_.queue())
        sendMessageInArrivalChannel(guild, ArrivalMessage, member.getAsMention).foreach(_.queue())
      case (roles, maybeNickname) =>
        val selfMember = guild.getSelfMember
        val controller = guild.getController
        val roleAction = controller.modifyMemberRoles(member, roles.filter(selfMember.canInteract).asJava)
        val nicknameAction = maybeNickname.map(controller.setNickname(member, _))
        roleAction.reason("Restoring old roles after rejoining").queue()
        nicknameAction.foreach(_.reason("Restoring old nickname after rejoining").queue())
        sendMessageInArrivalChannel(guild, WelcomeBackMessage, member.getAsMention).foreach(_.queue())
    }
  }

  private def leave(event: GuildMemberLeaveEvent): Unit = {
    if (event.getUser.isBot) {
      // Don't bother with leave messages when it's just a bot.
      return
    }
    val guild = event.getGuild
    if (!bot.roleRestorer.isEnabledIn(DiscordId(guild))) {
      return
    }
    sendMessageInArrivalChannel(guild, LeaveMessage, event.getUser.getName).foreach(_.queue())
  }

  private def memberRolesAdded(event: GuildMemberRoleAddEvent): Unit = {
    if (bot.roleRestorer.isEnabledIn(DiscordId(event.getGuild))) {
      bot.roleRestorer.addUserRoles(event.getMember, event.getRoles.asScala)
    }
  }

  private def memberRolesRemoved(event: GuildMemberRoleRemoveEvent): Unit = {
    if (bot.roleRestorer.isEnabledIn(DiscordId(event.getGuild))) {
      bot.roleRestorer.removeUserRoles(event.getMember, event.getRoles.asScala)
    }
  }

  private def nickChanged(event: GuildMemberNickChangeEvent): Unit = {
    if (bot.roleRestorer.isEnabledIn(DiscordId(event.getGuild))) {
      bot.roleRestorer.changeNickname(event.getMember, Option(event.getNewNick))
    }
  }

  private def roleDeleted(event: RoleDeleteEvent): Unit = {
    val guildId = DiscordId(event.getGuild)
    val roleId = DiscordId(event.getRole)
    if (bot.roleRestorer.isEnabledIn(DiscordId(event.getGuild))) {
      bot.roleRestorer.forgetRole(guildId, roleId)
    }
    if (bot.roleCategorizer.isEnabledIn(DiscordId(event.getGuild))) {
      bot.roleCategorizer.forgetRole(guildId, roleId)
    }
  }

  private def roleRenamed(event: RoleUpdateNameEvent): Unit = {
    if (bot.roleCategorizer.isEnabledIn(DiscordId(event.getGuild))) {
      bot.roleCategorizer.renameRole(event.getRole)
    }
  }

  private def messageReceived(event: MessageReceivedEvent): Unit = {
    // We're not interested in messages outside normal text channels.
    val channel = event.getTextChannel
    if (channel != null) {
      if (bot.messageLog.isEnabledIn(DiscordId(event.getGuild))) {
        bot.messageLog.logReceived(event.getMessage)
      }
      // Do not try to process bot messages.
      if (!event.getAuthor.isBot) {
        bot.commandProcessor.process(event.getMessage).foreach(_.queue())
      }
    }
  }

  private def messageUpdated(event: MessageUpdateEvent): Unit = {
    val channel = event.getTextChannel
    if (channel != null && bot.messageLog.isEnabledIn(DiscordId(event.getGuild))) {
      bot.messageLog.logUpdated(event.getMessage)
    }
  }

  private def messageDeleted(event: MessageDeleteEvent): Unit = {
    val channel = event.getTextChannel
    if (channel != null && bot.messageLog.isEnabledIn(DiscordId(event.getGuild))) {
      bot.messageLog.logRemoved(channel, new DiscordId(event.getMessageIdLong))
    }
  }

  private def messagesDeleted(event: MessageBulkDeleteEvent): Unit = {
    if (bot.messageLog.isEnabledIn(DiscordId(event.getGuild))) {
      bot.messageLog.logBulkRemoved(
        event.getChannel,
        event.getMessageIds.asScala.map(rawId => new DiscordId(rawId.toLong)))
    }
  }

  private def addAutorole(guild: Guild, member: Member): Option[RestAction[_]] = {
    for {
      roleId <- bot.guildSettings(DiscordId(guild), Autorole)
      role <- Option(guild.getRoleById(roleId.value))
    } yield guild.getController.addRolesToMember(member, role).reason("Autorole")
  }

  private def sendMessageInArrivalChannel(guild: Guild, msg: GuildSetting[String],
    userName: String): Option[RestAction[_]] = {
    val guildId = DiscordId(guild)
    for {
      channelId <- bot.guildSettings(guildId, ArrivalChannel)
      message <- bot.guildSettings(guildId, msg)
      channel <- Option(guild.getTextChannelById(channelId.value))
    } yield channel.sendMessage(message.replaceAllLiterally("${USER}", userName))
  }

  private def setPresence(jda: JDA): Unit = {
    jda.getPresence.setGame(Game.playing(bot.commandProcessor.getHelpCommand))
  }
}
