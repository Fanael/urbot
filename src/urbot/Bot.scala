// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot

import urbot.commands.CommandProcessor
import urbot.settings.GuildSettings

private class Bot(val databaseConnection: DatabaseConnection, val commandPrefix: String) extends AlwaysEnabledModule {
  val libraryController = new LibraryController(databaseConnection)
  val guildSettings = new GuildSettings(databaseConnection)
  val messageLog = new MessageLog(databaseConnection)
  val roleCategorizer = new RoleCategorizer(databaseConnection)
  val roleRestorer = new RoleRestorer(databaseConnection)
  val commandProcessor = new CommandProcessor(this)

  val moduleNameMap: Map[String, PersistedModule] = {
    val moduleClass = classOf[PersistedModule]
    val moduleFields = this.getClass.getDeclaredFields.filter(field => moduleClass.isAssignableFrom(field.getType))
    val namedModules = moduleFields.map(_.get(this).asInstanceOf[PersistedModule])
    val moduleNameMap = namedModules.map(module => (module.name, module)).toMap
    assert(namedModules.length == moduleNameMap.size, "Modules lost during conversion to map?")
    moduleNameMap
  }
}
