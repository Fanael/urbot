// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot.commands

import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.TextChannel
import net.dv8tion.jda.core.requests.RestAction
import urbot.DiscordId
import urbot.RoleCategorizer

private[commands] object RemoveCategoryCommand extends MemberFilter.RoleManagementCapable with CommandModule.Roles {
  override def parse(categorizer: RoleCategorizer, message: Message, extractor: FieldExtractor): ParseResult = {
    val categoryName = extractor.nextField().getOrElse(return ParseFailure("Category name is required."))
    if (extractor.nextField().isDefined) {
      return ParseFailure("Only one category name accepted at a time.")
    }
    val guildId = DiscordId(message.getGuild)
    new CommandAction {
      override def apply(channel: TextChannel, authorMention: String): Option[RestAction[_]] = {
        if (categorizer.removeCategory(guildId, categoryName)) {
          Some(channel.sendMessage(s"${authorMention}, role category `${categoryName}` removed successfully."))
        } else {
          Some(channel.sendMessage(s"${authorMention}, role category `${categoryName}` not found, nothing removed."))
        }
      }
    }
  }

  override final val description =
    """Remove a role category.

This command takes a single argument `<category-name>`, being the name of the role category to remove.

The roles in the category are forgotten as if by `forget-role`, so they still exist on the server.

Example of use:
 - `${COMMAND} foo`: remove the role category `foo`, forgetting all roles in it."""
}
