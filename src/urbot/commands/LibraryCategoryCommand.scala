// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot.commands

import net.dv8tion.jda.core.entities.Guild
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.TextChannel
import urbot.DiscordId
import urbot.LibraryController

private[this] trait LibraryCategoryCommand extends MemberFilter.ModOnly with CommandModule.Library {
  override final def parse(library: LibraryController, message: Message, extractor: FieldExtractor): ParseResult = {
    val categoryName = extractor.nextField().getOrElse(return ParseFailure("Category name is required."))
    if (extractor.nextField().isDefined) {
      ParseFailure("Category name cannot contain spaces.")
    } else {
      makeAction(library, DiscordId(message.getGuild), categoryName)
    }
  }

  protected def makeAction(library: LibraryController, guildId: DiscordId[Guild], categoryName: String): CommandAction
}

private[commands] object LibraryCategoryCommand {

  object Add extends LibraryCategoryCommand {
    override def makeAction(library: LibraryController, guildId: DiscordId[Guild],
      categoryName: String): CommandAction = (channel: TextChannel, authorMention: String) => {
      library.add(guildId, categoryName)
      Some(channel.sendMessage(s"${authorMention}, library category `${categoryName}` created successfully."))
    }

    override final val description =
      """Add a library category.

This commands accept a single argument `<category-name>`, which is the name of the category to create.
The category name must contain no spaces.

Example of use:
 - `${COMMAND} foo`: create a library category named `foo`."""
  }

  object Remove extends LibraryCategoryCommand {
    override def makeAction(library: LibraryController, guildId: DiscordId[Guild],
      categoryName: String): CommandAction = (channel: TextChannel, authorMention: String) => {
      if (library.remove(guildId, categoryName)) {
        Some(channel.sendMessage(s"${authorMention}, library category `${categoryName}` removed successfully."))
      } else {
        Some(channel.sendMessage(
          s"${authorMention}, library category `${categoryName}` not found, nothing removed."))
      }
    }

    override final val description =
      """Remove a library category.

This commands accept a single argument `<category-name>`, which is the name of the category to remove.
All entries in the specified categories are removed.

Example of use:
 - `${COMMAND} foo`: remove a library category named `foo` and all its entries."""
  }

}
