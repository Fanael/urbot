// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot.commands

import net.dv8tion.jda.core.entities.Guild
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.TextChannel
import urbot.DiscordId
import urbot.RoleCategorizer

private[this] trait RoleCategoryCommand extends MemberFilter.RoleManagementCapable with CommandModule.Roles {
  protected def makeAction(categorizer: RoleCategorizer, guildId: DiscordId[Guild], categoryName: String,
    description: String): CommandAction

  override final def parse(categorizer: RoleCategorizer, message: Message, extractor: FieldExtractor): ParseResult = {
    val categoryName = extractor.nextField().getOrElse(return ParseFailure("Category name is required."))
    val description = extractor.getRest
    makeAction(categorizer, DiscordId(message.getGuild), categoryName, description)
  }
}

private[commands] object RoleCategoryCommand {

  object Add extends RoleCategoryCommand {
    protected override def makeAction(categorizer: RoleCategorizer, guildId: DiscordId[Guild], categoryName: String,
      description: String): CommandAction = (channel: TextChannel, authorMention: String) => {
      if (categorizer.addCategory(guildId, categoryName, description)) {
        Some(channel.sendMessage(s"${authorMention}, role category `${categoryName}` added successfully."))
      } else {
        Some(channel.sendMessage(s"${authorMention}, role category `${categoryName}` already exists."))
      }
    }

    override final val description =
      """Create a new role category.

This command takes two arguments `<category-name> <description>`.
Category names must be unique - it's not possible to have two categories with the same name on a server.

Example of use:
 - `${COMMAND} foo Roles for those who like foo`: creates role category `foo`."""
  }

  object Update extends RoleCategoryCommand {
    protected override def makeAction(categorizer: RoleCategorizer, guildId: DiscordId[Guild], categoryName: String,
      description: String): CommandAction = (channel: TextChannel, authorMention: String) => {
      if (categorizer.updateCategory(guildId, categoryName, description)) {
        Some(channel.sendMessage(s"${authorMention}, role category `${categoryName}` updated successfully."))
      } else {
        Some(channel.sendMessage(s"${authorMention}, role category `${categoryName}` not found, nothing changed."))
      }
    }

    override final val description =
      """Update a role category's description.

This command takes two arguments `<category-name> <description>`.

Example of use:
 - `${COMMAND} foo Roles for those who like foo more than bar`: updates the description of category `foo`."""
  }

}
