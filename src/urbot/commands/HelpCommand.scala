// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot.commands

import net.dv8tion.jda.core.entities.Guild
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.TextChannel
import net.dv8tion.jda.core.requests.RestAction
import urbot.Bot
import urbot.DiscordId

private[this] trait HelpCommand extends CommandModule.WholeBot {
  protected def commandFilter(command: Command): Boolean

  override final def parse(bot: Bot, message: Message, extractor: FieldExtractor): ParseResult = {
    val commandName = extractor.nextField().getOrElse(
      return new ShowCommandListAction(bot, DiscordId(message.getGuild)))
    if (extractor.nextField().isDefined) {
      return ParseFailure("Only one command name allowed.")
    }
    val command = bot.commandProcessor.commandMap.getOrElse(commandName,
      return ParseFailure(s"Unknown command name: `${commandName}`."))
    if (!command.memberAllowed(Option(message.getMember))) {
      ParseFailure(s"You lack the required permissions to use `${commandName}`.")
    } else {
      new ShowHelpForCommandAction(bot, commandName, command)
    }
  }

  private class ShowCommandListAction(bot: Bot, guildId: DiscordId[Guild]) extends CommandAction {
    override def apply(channel: TextChannel, authorMention: String): Option[RestAction[_]] = {
      def shouldShowCommand(tuple: (String, Command)): Boolean = {
        val command = tuple._2
        commandFilter(command) && command.getAssociatedModule(bot).isEnabledIn(guildId)
      }

      val commandList = bot.commandProcessor.commands.view.filter(shouldShowCommand).map(_._1).mkString("\n")
      Some(channel.sendMessage("Command list: ```\n" + commandList + "```"))
    }
  }

  private final class ShowHelpForCommandAction(bot: Bot, name: String, command: Command) extends CommandAction {
    override def apply(channel: TextChannel, authorMention: String): Option[RestAction[_]] = {
      val prefixedName = bot.commandProcessor.commandPrefix + name
      val help = command.description.replaceAllLiterally("${COMMAND}", prefixedName)
      Some(channel.sendMessage(s"${authorMention}, help for `${prefixedName}`:\n${help}"))
    }
  }

}

private[commands] object HelpCommand {

  object Common extends HelpCommand with MemberFilter.Any {
    override def commandFilter(command: Command): Boolean = command.isInstanceOf[MemberFilter.Any]

    override final val description: String =
      """Show help message.

With no arguments, show the list of commands available to everyone on this server.

With a single argument `<command-name>`, show detailed help on the given command.

Examples of use:
 - `${COMMAND}`: list all commands.
 - `${COMMAND} ping`: show the detailed help on the `ping` command."""
  }

  object Mod extends HelpCommand with MemberFilter.ModOnly {
    override def commandFilter(command: Command): Boolean = !command.isInstanceOf[MemberFilter.Any]

    override final val description =
      """Show mod-specific help message.

With no arguments, show the list of commands available only to moderators on this server.

With a single argument `<command-name>`, show detailed help on the given command, just like `help`.

Examples of use:
 - `${COMMAND}`: list all moderator-restricted commands.
 - `${COMMAND} ping`: show the detailed help on the `ping` command."""
  }

}
