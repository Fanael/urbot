// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot.commands

import urbot.AlwaysEnabledModule
import urbot.Bot
import urbot.LibraryController
import urbot.MessageLog
import urbot.RoleCategorizer
import urbot.settings.GuildSettings

private[commands] object CommandModule {

  trait None extends Command {
    override type AssociatedModule = AlwaysEnabledModule

    final override def getAssociatedModule(bot: Bot): AssociatedModule = AlwaysEnabledModule
  }

  trait Library extends Command {
    override type AssociatedModule = LibraryController

    final override def getAssociatedModule(bot: Bot): AssociatedModule = bot.libraryController
  }

  trait Settings extends Command {
    override type AssociatedModule = GuildSettings

    final override def getAssociatedModule(bot: Bot): AssociatedModule = bot.guildSettings
  }

  trait Log extends Command {
    override type AssociatedModule = MessageLog

    final override def getAssociatedModule(bot: Bot): AssociatedModule = bot.messageLog
  }

  trait Roles extends Command {
    override type AssociatedModule = RoleCategorizer

    final override def getAssociatedModule(bot: Bot): AssociatedModule = bot.roleCategorizer
  }

  trait WholeBot extends Command {
    override type AssociatedModule = Bot

    final override def getAssociatedModule(bot: Bot): AssociatedModule = bot
  }

}
