// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot.commands

import java.nio.charset.StandardCharsets

import net.dv8tion.jda.core.entities.Guild
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.TextChannel
import net.dv8tion.jda.core.requests.RestAction
import urbot.PermissionDumper

private[commands] object DumpAclCommand extends MemberFilter.RoleManagementCapable with CommandModule.None {
  override def parse(_m: AssociatedModule, message: Message, extractor: FieldExtractor): ParseResult = {
    extractor.nextField() match {
      case Some(_) => ParseFailure("This command takes no arguments.")
      case None => new Action(message.getGuild)
    }
  }

  private final class Action(guild: Guild) extends CommandAction {
    override def apply(channel: TextChannel, authorMention: String): Option[RestAction[_]] = {
      val dumper = new PermissionDumper
      val dump = dumper(guild).getBytes(StandardCharsets.UTF_8)
      Some(channel.sendFile(dump, "acl-dump.json"))
    }
  }

  override final val description =
    """Dump all access control lists on this server.

This commands creates a user-readable dump of all channel, role and user permissions on this server.
It's quite slow, so try to avoid excessive usage of this command.

Example of use:
 - `${COMMAND}`"""
}
