// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot.commands

import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.TextChannel
import net.dv8tion.jda.core.requests.RestAction
import urbot.DiscordId
import urbot.LibraryController

private[commands] object LibraryRemoveTopicCommand extends MemberFilter.ModOnly with CommandModule.Library {
  override def parse(library: LibraryController, message: Message, extractor: FieldExtractor): ParseResult = {
    val categoryName = extractor.nextField().getOrElse(return ParseFailure("Category name is required."))
    val topicName = extractor.nextField().getOrElse(return ParseFailure("Topic name is required."))
    if (extractor.nextField().isDefined) {
      return ParseFailure("Extranous argument after topic name.")
    }
    val category = library(DiscordId(message.getGuild), categoryName).getOrElse(
      return ParseFailure("Library category `" + categoryName + "` not found, cannot remove entry."))
    new CommandAction {
      override def apply(channel: TextChannel, authorMention: String): Option[RestAction[_]] = {
        category.remove(topicName)
        Some(channel.sendMessage(s"Entry `${topicName}` in category `${categoryName}` removed successfully."))
      }
    }
  }

  override final val description =
    """Remove a library topic.

This command accepts two arguments `<category-name> <topic-name>`.
`<category-name>` is the category in which to remove the given topic.
`<topic-name>` is the name of the topic to remove.

Example of use:
 - `${COMMAND} foo test`: remove the topic `test` in category `foo`."""
}
