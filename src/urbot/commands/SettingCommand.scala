// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot.commands

import net.dv8tion.jda.core.entities.Guild
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.Role
import net.dv8tion.jda.core.entities.TextChannel
import net.dv8tion.jda.core.requests.RestAction
import urbot.DiscordId
import urbot.settings.GuildSetting
import urbot.settings.GuildSettings
import urbot.settings.RoleSetting
import urbot.settings.StringSetting
import urbot.settings.TextChannelSetting

private[urbot] object SettingCommand extends MemberFilter.SettingCapable with CommandModule.Settings {
  override def parse(settings: GuildSettings, message: Message, extractor: FieldExtractor): ParseResult = {
    val settingName = extractor.nextField().getOrElse(return ListSettingsAction)
    val setting = GuildSettings.settingRegistry.getOrElse(settingName,
      return ParseFailure(s"Unknown setting name `${settingName}`."))
    val rest = extractor.getRest
    if (rest.isEmpty) {
      new ShowSettingAction(settings, setting)
    } else {
      setting.parseSetting(settings, message, rest)
    }
  }

  private[urbot] def parseSetting(settings: GuildSettings, message: Message, newValue: String,
    setting: StringSetting): ParseResult = {
    new SetStringSettingAction(settings, DiscordId(message.getGuild), setting, newValue)
  }

  private[urbot] def parseSetting(settings: GuildSettings, message: Message,
    setting: TextChannelSetting): ParseResult = {
    val mentionedChannels = message.getMentionedChannels
    mentionedChannels.size match {
      case 0 => ParseFailure("A channel mention is required.")
      case 1 =>
        new SetTextChannelSettingAction(
          settings, DiscordId(message.getGuild), setting, DiscordId(mentionedChannels.get(0)))
      case _ => ParseFailure("Only one channel may be mentioned.")
    }
  }

  private[urbot] def parseSetting(settings: GuildSettings, message: Message, setting: RoleSetting): ParseResult = {
    val mentionedRoles = message.getMentionedRoles
    mentionedRoles.size match {
      case 0 => ParseFailure("A role mention is required.")
      case 1 =>
        new SetRoleSettingAction(settings, DiscordId(message.getGuild), setting, DiscordId(mentionedRoles.get(0)))
      case _ => ParseFailure("Only one role may be mentioned.")
    }
  }

  private object ListSettingsAction extends CommandAction {
    override def apply(channel: TextChannel, authorMention: String): Option[RestAction[_]] = {
      val names = GuildSettings.settingRegistry.keys.toVector.sorted.mkString("\n")
      Some(channel.sendMessage("```\n" + names + "```"))
    }
  }

  private final class ShowSettingAction(settings: GuildSettings, setting: GuildSetting[_]) extends CommandAction {
    override def apply(channel: TextChannel, authorMention: String): Option[RestAction[_]] = {
      Some(setting.show(settings, channel))
    }
  }

  private final class SetStringSettingAction(settings: GuildSettings, guildId: DiscordId[Guild], setting: StringSetting,
    newValue: String) extends CommandAction {
    override def apply(channel: TextChannel, authorMention: String): Option[RestAction[_]] = {
      settings(guildId, setting) = newValue
      Some(channel.sendMessage(s"`${setting.name}` changed successfully."))
    }
  }

  private final class SetTextChannelSettingAction(settings: GuildSettings, guildId: DiscordId[Guild],
    setting: TextChannelSetting, newValue: DiscordId[TextChannel]) extends CommandAction {
    override def apply(channel: TextChannel, authorMention: String): Option[RestAction[_]] = {
      settings(guildId, setting) = newValue
      Some(channel.sendMessage(s"`${setting.name}` changed successfully."))
    }
  }

  private final class SetRoleSettingAction(settings: GuildSettings, guildId: DiscordId[Guild], setting: RoleSetting,
    newValue: DiscordId[Role]) extends CommandAction {
    override def apply(channel: TextChannel, authorMention: String): Option[RestAction[_]] = {
      settings(guildId, setting) = newValue
      Some(channel.sendMessage(s"`${setting.name}` changed successfully."))
    }
  }

  private[urbot] def show(settings: GuildSettings, channel: TextChannel, setting: StringSetting): RestAction[_] = {
    settings(DiscordId(channel.getGuild), setting) match {
      case Some(value) => channel.sendMessage(s"Current value of `${setting.name}`: ${value}")
      case None => channel.sendMessage(s"`${setting.name}` is not set.")
    }
  }

  private[urbot] def show(settings: GuildSettings, channel: TextChannel, setting: TextChannelSetting): RestAction[_] = {
    settings(DiscordId(channel.getGuild), setting) match {
      case Some(id) => channel.sendMessage(s"Current value of ${setting.name}: <#${id.value}>.")
      case None => channel.sendMessage(s"`${setting.name}` is not set.")
    }
  }

  private[urbot] def show(settings: GuildSettings, channel: TextChannel, setting: RoleSetting): RestAction[_] = {
    settings(DiscordId(channel.getGuild), setting) match {
      case Some(id) => channel.sendMessage(s"Current value of ${setting.name}: <@&${id.value}>.")
      case None => channel.sendMessage(s"`${setting.name}` is not set.")
    }
  }

  override final val description =
    """Get, set or list guild settings.

With no arguments, list all guild setting names.

With a single argument `<setting-name>`, get the value of the given setting on this guild.

With two arguments `<setting-name> <new-value>`, set the value of the given setting to `new-value`.

Examples of use:
 - `${COMMAND}`: list all setting names.
 - `${COMMAND} autorole`: show the current autorole on this server.
 - `${COMMAND} welcome-back-message Welcome back ${USER}!`: set the new value of welcome back message."""
}
