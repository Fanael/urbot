// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot.commands

import net.dv8tion.jda.core.entities.Guild
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.TextChannel
import net.dv8tion.jda.core.requests.RestAction
import urbot.DiscordId
import urbot.RoleCategorizer

private[commands] object ListRolesCommand extends MemberFilter.Any with CommandModule.Roles {
  override def parse(categorizer: RoleCategorizer, message: Message, extractor: FieldExtractor): ParseResult = {
    val guildId = DiscordId(message.getGuild)
    val categoryName = extractor.nextField().getOrElse(return new ListCategoriesAction(categorizer, guildId))
    if (extractor.nextField().isDefined) {
      ParseFailure("Only one category name allowed.")
    } else {
      new ListRolesAction(categorizer, guildId, categoryName)
    }
  }

  private final class ListCategoriesAction(categorizer: RoleCategorizer, guildId: DiscordId[Guild])
    extends CommandAction {

    override def apply(channel: TextChannel, authorMention: String): Option[RestAction[_]] = {
      val categories = categorizer.getRoleCategories(guildId)
      val builder = StringBuilder.newBuilder
      categories.foreach(formatCategory(builder, _))
      Some(channel.sendMessage("Existing role categories:\n" + builder.toString))
    }

    private def formatCategory(builder: StringBuilder, tuple: (String, String)): Unit = {
      builder.append('`')
      builder.append(tuple._1)
      builder.append("`: ")
      builder.append(tuple._2)
      builder.append('\n')
    }
  }

  private final class ListRolesAction(categorizer: RoleCategorizer, guildId: DiscordId[Guild], categoryName: String)
    extends CommandAction {

    override def apply(channel: TextChannel, authorMention: String): Option[RestAction[_]] = {
      categorizer.getRolesInCategory(guildId, categoryName) match {
        case None => Some(channel.sendMessage(s"${authorMention}, category `${categoryName}` not found."))
        case Some(roleNames) =>
          val builder = StringBuilder.newBuilder
          roleNames.foreach { name =>
            builder.append(name)
            builder.append('\n')
          }
          Some(channel.sendMessage("```\n" + builder.toString + "```"))
      }
    }
  }

  override final val description =
    """List role categories or roles in a category.

With no arguments, this command shows a list of all role categories.

With a single argument `<role-category>`, it shows a list of roles in the given category.

Example of use:
 - `${COMMAND}`: list all role categories on this server.
 - `${COMMAND} foo`: list all roles in category `foo` on this server."""
}
