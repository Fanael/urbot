// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot.commands

// This class extracts space-separated fields from a char sequence.
// One of the problems of the old command processor was that its parsing was
// very ad hoc, so let's not repeat the same mistake and have a common field
// extracting parser in one place.
private class FieldExtractor(sequence: CharSequence) {
  // Get the next non-empty field or None.
  def nextField(): Option[String] = {
    skipSpaces()
    val fieldStart = index
    skipNonSpaces()
    val fieldEnd = index
    if (fieldStart == fieldEnd) {
      None
    } else {
      Some(sequence.subSequence(fieldStart, fieldEnd).toString)
    }
  }

  // Return the remaining fields as a single field, trimming whitespace
  // on both ends, but keeping internal whitespace untouched.
  def getRest: String = {
    sequence.subSequence(index, sequence.length).toString.trim()
  }

  private def skipSpaces(): Unit = skip(_ == ' ')

  private def skipNonSpaces(): Unit = skip(_ != ' ')

  private def skip(predicate: Char => Boolean): Unit = {
    while (index < sequence.length && predicate(sequence.charAt(index))) {
      index += 1
    }
  }

  private var index: Int = 0
}
