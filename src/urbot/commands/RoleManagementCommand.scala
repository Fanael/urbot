// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot.commands

import scala.util.Failure
import scala.util.Success

import net.dv8tion.jda.core.entities.Guild
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.TextChannel
import urbot.DiscordId
import urbot.ImportResult
import urbot.RoleCategorizer

private[this] trait RoleManagementCommand extends MemberFilter.RoleManagementCapable with CommandModule.Roles {
  protected def makeAction(categorizer: RoleCategorizer, guild: Guild, categoryName: String,
    roleName: String): CommandAction

  override def parse(categorizer: RoleCategorizer, message: Message, extractor: FieldExtractor): ParseResult = {
    val categoryName = extractor.nextField().getOrElse(return ParseFailure("Category name is required."))
    val roleName = extractor.getRest
    if (roleName.isEmpty) {
      ParseFailure("Role name is required.")
    } else {
      makeAction(categorizer, message.getGuild, categoryName, roleName)
    }
  }
}

private[commands] object RoleManagementCommand {

  object Import extends RoleManagementCommand {
    protected override def makeAction(categorizer: RoleCategorizer, guild: Guild, categoryName: String,
      roleName: String): CommandAction = (channel: TextChannel, authorMention: String) => {
      categorizer.importRole(guild, categoryName, roleName) match {
        case ImportResult.MultipleRolesFound =>
          Some(channel.sendMessage(s"${authorMention}, multiple roles named `${roleName}` exist on the server."))
        case ImportResult.CategoryNotFound =>
          Some(channel.sendMessage(s"${authorMention}, category `${categoryName}` doesn't exist."))
        case ImportResult.RoleNotFound =>
          Some(channel.sendMessage(s"${authorMention}, role `${roleName}` not found on the server."))
        case ImportResult.Ok =>
          Some(channel.sendMessage(s"${authorMention}, role `${roleName}` imported successfully."))
      }
    }

    override final val description =
      """Import a preexisting role into a role category.

This command takes two arguments, `<category-name>` `<role-name>`, where `<category-name>` is the name of the
role category to add the role to, and `<role-name>` is the role name (not mention!) to import.

Example of use:
 - `${COMMAND} foo Foo Level 1`: import the role \"Foo Level 1\" into category `foo`."""
  }

  object Add extends RoleManagementCommand {
    protected override def makeAction(categorizer: RoleCategorizer, guild: Guild, categoryName: String,
      roleName: String): CommandAction = (channel: TextChannel, authorMention: String) => {
      categorizer.addRole(guild, categoryName, roleName) { insertResult =>
        val msg = insertResult match {
          case Failure(exc) =>
            s"${authorMention}, error adding role (role created on the server nonetheless): ${exc.getMessage}"
          case Success(_) =>
            s"${authorMention}, role `${roleName}` in category `${categoryName}` created successfully."
        }
        channel.sendMessage(msg).queue()
      }
      None
    }

    override final val description =
      """Create a new role in a category.

This command takes two arguments, `<category-name>` `<role-name>`, where `<category-name>` is the name of the
role category to add the role to, and `<role-name>` is the role name (not mention!) to create.

This command differs from `import-role` in that it *creates* a new role on the server.

Example of use:
 - `${COMMAND} foo Foo Level 3`: create a new role named \"Foo Level 3\" on the server and add it to category `foo`."""
  }

  object Forget extends RoleManagementCommand {
    protected override def makeAction(categorizer: RoleCategorizer, guild: Guild, categoryName: String,
      roleName: String): CommandAction = {
      val guildId = DiscordId(guild)
      (channel: TextChannel, authorMention: String) => {
        if (categorizer.forgetRole(guildId, categoryName, roleName)) {
          Some(channel.sendMessage(
            s"${authorMention}, role `${roleName}` in `${categoryName}` forgotten successfully."))
        } else {
          Some(channel.sendMessage(authorMention + ", no such role and/or category, nothing removed."))
        }
      }
    }

    override final val description =
      """Drop a role from a category.

This command takes two arguments, `<category-name>` `<role-name>`, where `<category-name>` is the name of the
role category to remove the role from, and `<role-name>` is the role name (not mention!) to drop.

The role is only dropped from the bot's internal database, it will still exist on the server.

Example of use:
 - `${COMMAND} foo Foo Level 2`: forget the role \"Foo Level 2\" from category `foo`."""
  }

}
