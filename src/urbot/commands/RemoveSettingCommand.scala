// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot.commands

import net.dv8tion.jda.core.entities.Guild
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.TextChannel
import net.dv8tion.jda.core.requests.RestAction
import urbot.DiscordId
import urbot.settings.GuildSetting
import urbot.settings.GuildSettings

private[commands] object RemoveSettingCommand extends MemberFilter.SettingCapable with CommandModule.Settings {
  override def parse(settings: GuildSettings, message: Message, extractor: FieldExtractor): ParseResult = {
    val settingName = extractor.nextField().getOrElse(return ParseFailure("Setting name is required."))
    if (extractor.nextField().isDefined) {
      return ParseFailure("Only one setting name allowed.")
    }
    val setting = GuildSettings.settingRegistry.getOrElse(settingName,
      return ParseFailure(s"Unknown setting name `${settingName}`."))
    new RemoveSettingAction(settings, DiscordId(message.getGuild), setting)
  }

  private final class RemoveSettingAction(settings: GuildSettings, guildId: DiscordId[Guild], setting: GuildSetting[_])
    extends CommandAction {

    override def apply(channel: TextChannel, authorMention: String): Option[RestAction[_]] = {
      settings.remove(guildId, setting)
      Some(channel.sendMessage(s"${authorMention}, setting removed successfully."))
    }
  }

  override final val description =
    """Remove the given guild setting. This typically disables the related functionality.
Expects one argument `<setting-name>`.

Example of use:
 - `${COMMAND} autorole`: removes the autorole setting on this server, disabling the autorole functionality."""
}
