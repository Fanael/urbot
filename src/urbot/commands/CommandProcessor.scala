// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot.commands

import scala.util.Try

import com.typesafe.scalalogging.Logger
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.requests.RestAction
import urbot.Bot
import urbot.DiscordId
import urbot.Module
import urbot.PersistedModule

private[urbot] class CommandProcessor(bot: Bot) {
  def getHelpCommand: String = commandPrefix + CommandProcessor.HELP_COMMAND_NAME

  def process(message: Message): Option[RestAction[_]] = {
    val messageContent = message.getContentRaw
    if (!messageContent.startsWith(commandPrefix)) {
      return None
    }
    val fieldExtractor = new FieldExtractor(messageContent)
    val commandName = fieldExtractor.nextField().get.substring(commandPrefix.length)
    commandMap.get(commandName) match {
      case None => handleUnknownCommand(message, commandName)
      case Some(command) => handleKnownCommand(message, command, fieldExtractor)
    }
  }

  private def handleUnknownCommand(message: Message, commandName: String): Option[RestAction[_]] = {
    if (commandName.codePoints().noneMatch(Character.isLetter)) {
      // The alleged command name contains no letters at all, so likely not
      // intended to be a command in the first place.
      return None
    }
    val channel = message.getTextChannel
    val authorMention = message.getMember.getAsMention
    val helpCommandName = CommandProcessor.HELP_COMMAND_NAME
    Some(channel.sendMessage(s"${authorMention}, unknown command `${commandName}`, try `${helpCommandName}`."))
  }

  private def handleKnownCommand(message: Message, command: Command, fields: FieldExtractor): Option[RestAction[_]] = {
    val channel = message.getTextChannel
    Try {
      val messageId = DiscordId(message)
      val authorMention = message.getAuthor.getAsMention
      if (!command.memberAllowed(Option(message.getMember))) {
        logger.debug("Permission check failed for message {}", messageId)
        Some(channel.sendMessage(authorMention + ", you lack the required permissions for this action."))
      } else {
        val module = command.getAssociatedModule(bot)
        if (!module.isEnabledIn(DiscordId(message.getGuild))) {
          Some(channel.sendMessage(authorMention + ", " + getModuleDisabledMessage(module)))
        } else {
          command.parse(module, message, fields) match {
            case ParseFailure(errorMessage) => Some(channel.sendMessage(authorMention + ", error: " + errorMessage))
            case action: CommandAction => action(channel, authorMention)
          }
        }
      }
    }.recover {
      case e =>
        logger.error("Command processing error", e)
        Some(channel.sendMessage("Internal error: " + e.getMessage))
    }.get
  }

  private def getModuleDisabledMessage(module: Module): String = {
    module match {
      case namedModule: PersistedModule => "the module `" + namedModule.name + "` is disabled on this server."
      case _ => "this command is disabled on this server."
    }
  }

  private[commands] val commandPrefix = bot.commandPrefix
  private[commands] val commands = CommandProcessor.commands
  private[commands] val commandMap = commands.toMap
  assert(commands.length == commandMap.size, "Commands were lost during conversion to map?")
  private val logger = Logger[CommandProcessor]
}

private[this] object CommandProcessor {
  private final val HELP_COMMAND_NAME = "help"

  private val commands = Array[(String, Command)](
    // Common commands.
    (HELP_COMMAND_NAME, HelpCommand.Common),
    ("ping", PingCommand),
    // Mod-specific commands.
    ("mod-help", HelpCommand.Mod),
    ("setting", SettingCommand),
    ("remove-setting", RemoveSettingCommand),
    ("list-modules", ListModulesCommand),
    ("enable", ModuleManagementCommand.Enable),
    ("disable", ModuleManagementCommand.Disable),
    ("dump-acl", DumpAclCommand),
    ("get-log", GetLogCommand),
    // Library commands.
    ("library-add-category", LibraryCategoryCommand.Add),
    ("library-remove-category", LibraryCategoryCommand.Remove),
    ("library-set-topic", LibrarySetTopicCommand),
    ("library-remove-topic", LibraryRemoveTopicCommand),
    ("library", LibraryCommand),
    // Role commands.
    ("add-category", RoleCategoryCommand.Add),
    ("update-category", RoleCategoryCommand.Update),
    ("remove-category", RemoveCategoryCommand),
    ("import-role", RoleManagementCommand.Import),
    ("add-role", RoleManagementCommand.Add),
    ("forget-role", RoleManagementCommand.Forget),
    ("roles", ListRolesCommand),
    ("ranks", ListRolesCommand),
    ("category", ListRolesCommand),
    ("r", RankCommand),
    ("rank", RankCommand),
    ("role", RoleCommand))
}
