// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot

import java.util.UUID

import scala.collection.JavaConverters.asScalaBufferConverter

import net.dv8tion.jda.core.entities.Guild
import urbot.JDBCExtensions.BindableStatement

private class GuildRolesTemporaryTable(databaseConnection: DatabaseConnection, guild: Guild) extends AutoCloseable {
  override def close(): Unit = {
    databaseConnection.withStatement(s"DROP TABLE ${name}")(_.execute())
  }

  val name: String = {
    val uuid = UUID.randomUUID()
    f"temp.roles${uuid.getMostSignificantBits}%016X${uuid.getLeastSignificantBits}%016X"
  }
  GuildRolesTemporaryTable.initialize(name, databaseConnection, guild)
}

private[this] object GuildRolesTemporaryTable {
  private def initialize(tableName: String, databaseConnection: DatabaseConnection, guild: Guild): Unit = {
    val sql = s"CREATE TABLE IF NOT EXISTS ${tableName} (role_id INTEGER NOT NULL PRIMARY KEY) WITHOUT ROWID"
    databaseConnection.withStatement(sql)(_.execute())
    databaseConnection.withStatement(s"INSERT INTO ${tableName} VALUES (?)") { insertStmt =>
      guild.getRoles.asScala.foreach { role =>
        insertStmt.bind(DiscordId(role))
        insertStmt.executeUpdate()
      }
    }
  }
}
