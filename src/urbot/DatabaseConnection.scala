// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot

import java.sql.Connection
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantReadWriteLock

import scala.collection.mutable
import scala.util.Try

import com.typesafe.scalalogging.Logger
import org.sqlite.SQLiteConfig
import org.sqlite.SQLiteOpenMode
import org.sqlite.core.CoreStatement
import urbot.JDBCExtensions.ExtendedResultSet

private class DatabaseConnection(databaseFilePath: String, instancesPending: Int) {
  // Get a cached SQL prepared statement, blocking the periodic commit thread
  // completely until the callback returns. Other threads can still run
  // concurrently.
  def withStatement[A](sql: String)(func: PreparedStatement => A): A = {
    // We can do this outside the database lock, so the periodic commit thread
    // and statement fetch/preparation can run concurrently.
    val statement = getCachedStatement(sql)
    Utils.withResource(new DatabaseConnection.CloseableLock(rwLock.readLock())) { _ => func(statement) }
  }

  def startPeriodicCommit(): Unit = {
    if (instancesLeftToStart.decrementAndGet() <= 0) {
      if (periodicCommitExecutor.isDefined) {
        // Already started.
        return
      }
      // We're the last thread, so commit whatever the initialization stage
      // changed, then start the periodic commit thread.
      connection.commit()
      periodicCommitExecutor = Some(DatabaseConnection.createPeriodicCommit(connection, logger, rwLock.writeLock()))
    }
  }

  private def getCachedStatement(sql: String): PreparedStatement = {
    val cache = statementCache.get()
    cache.get(sql) match {
      // Note: this is a hack that detects statements that autoclosed due
      // to an exception.
      case Some(statement) if statement.asInstanceOf[CoreStatement].pointer != 0 => statement
      case _ =>
        val result = prepare(sql)
        cache.update(sql, result)
        result
    }
  }

  private def prepare(sql: String): PreparedStatement = {
    logger.debug("Preparing statement {}", sql)
    if (logger.underlying.isDebugEnabled) {
      // Get query plan to see if we're not doing anything stupid.
      Utils.withResource(connection.createStatement()) { stmt =>
        Utils.withResource(stmt.executeQuery("EXPLAIN QUERY PLAN " + sql)) { rs =>
          def formatQueryPlanRow(rs: ResultSet): String = {
            s"| ${rs.getInt(1)} | ${rs.getInt(2)} | ${rs.getInt(3)} | ${rs.getString(4)} |"
          }

          val plan = rs.iterate(formatQueryPlanRow).mkString("\n")
          logger.debug("Query plan of {}:\n{}", sql, plan)
        }
      }
    }
    connection.prepareStatement(sql)
  }

  private val logger = Logger[DatabaseConnection]
  private val connection = DatabaseConnection.connect("jdbc:sqlite:" + databaseFilePath)
  private val rwLock = new ReentrantReadWriteLock(/* fair = */ true)
  private val instancesLeftToStart = new AtomicInteger(instancesPending)
  @volatile private var periodicCommitExecutor: Option[ScheduledExecutorService] = None
  private val statementCache = new ThreadLocal[mutable.HashMap[String, PreparedStatement]] {
    override def initialValue(): mutable.HashMap[String, PreparedStatement] = mutable.HashMap.empty
  }
}

private[this] object DatabaseConnection {

  private final class CloseableLock(private val lock: Lock) extends AutoCloseable {
    lock.lock()

    override def close(): Unit = {
      lock.unlock()
    }
  }

  private final val PERIODIC_COMMIT_MILLISECONDS = 10000

  private def createPeriodicCommit(connection: Connection, logger: Logger, lock: Lock): ScheduledExecutorService = {
    val executor = Executors.newScheduledThreadPool(1)
    val runnable = new Runnable {
      override def run(): Unit = {
        Utils.withResource(new CloseableLock(lock)) { _ =>
          Try(connection.commit()).recover {
            case exception: Exception => logger.error("Periodic commit failed: {}", exception)
          }
        }
      }
    }
    executor.scheduleAtFixedRate(runnable, PERIODIC_COMMIT_MILLISECONDS, PERIODIC_COMMIT_MILLISECONDS,
      TimeUnit.MILLISECONDS)
    // Remember to commit on SIGINT too.
    Runtime.getRuntime.addShutdownHook(new Thread(runnable))
    executor
  }

  private def connect(connectionString: String): Connection = {
    // Make sure we open the connection in FULLMUTEX mode so that it can be
    // accessed by multiple threads at the same time, as the periodic commit
    // runs in a separate thread.
    val config = new SQLiteConfig
    config.setOpenMode(SQLiteOpenMode.FULLMUTEX)
    val connection = config.createConnection(connectionString)
    setConnectionSettings(connection)
    connection.setAutoCommit(false)
    createTables(connection)
    connection.commit()
    connection
  }

  private def setConnectionSettings(connection: Connection): Unit = {
    Utils.withResource(connection.createStatement()) { stmt =>
      stmt.execute("PRAGMA encoding = \"UTF-8\"")
      stmt.execute("PRAGMA page_size = 8192")
      stmt.execute("PRAGMA automatic_index = OFF")
      stmt.execute("PRAGMA foreign_keys = ON")
      stmt.execute("PRAGMA secure_delete = OFF")
      stmt.execute("PRAGMA synchronous = FULL")
      stmt.execute("PRAGMA journal_mode = WAL")
      stmt.execute("PRAGMA journal_size_limit = 0")
      stmt.execute("PRAGMA cache_size = -16384")
    }
  }

  private def createTables(connection: Connection): Unit = {
    Utils.withResource(connection.createStatement()) { stmt =>
      stmt.execute(
        """CREATE TABLE IF NOT EXISTS role_categories (
  category_id INTEGER NOT NULL PRIMARY KEY,
  guild_id INTEGER NOT NULL,
  category_name TEXT NOT NULL COLLATE NOCASE,
  description TEXT NOT NULL,
  UNIQUE(guild_id, category_name)
)""")
      stmt.execute(
        """CREATE TABLE IF NOT EXISTS roles (
  category_id INTEGER NOT NULL REFERENCES role_categories(category_id) ON DELETE CASCADE,
  role_name TEXT NOT NULL COLLATE NOCASE,
  role_id INTEGER NOT NULL,
  PRIMARY KEY(category_id, role_name)
) WITHOUT ROWID""")
      stmt.execute("CREATE INDEX IF NOT EXISTS role_ids ON roles(role_id)")
      stmt.execute(
        """CREATE TABLE IF NOT EXISTS persisted_user_roles (
  guild_id INTEGER NOT NULL,
  user_id INTEGER NOT NULL,
  role_id INTEGER NOT NULL,
  PRIMARY KEY(guild_id, user_id, role_id)
) WITHOUT ROWID""")
      stmt.execute(
        """CREATE TABLE IF NOT EXISTS persisted_nicknames (
  guild_id INTEGER NOT NULL,
  user_id INTEGER NOT NULL,
  nickname TEXT NOT NULL,
  PRIMARY KEY(guild_id, user_id)
) WITHOUT ROWID""")
      stmt.execute(
        """CREATE TABLE IF NOT EXISTS guild_settings (
  guild_id INTEGER NOT NULL,
  name TEXT NOT NULL,
  value ANY NOT NULL,
  PRIMARY KEY(guild_id, name)
) WITHOUT ROWID""")
      stmt.execute(
        """CREATE TABLE IF NOT EXISTS library_categories (
  category_id INTEGER NOT NULL PRIMARY KEY,
  guild_id INTEGER NOT NULL,
  category_name TEXT NOT NULL COLLATE NOCASE,
  UNIQUE(guild_id, category_name)
)""")
      stmt.execute(
        """CREATE TABLE IF NOT EXISTS library_topics (
  category_id INTEGER NOT NULL REFERENCES library_categories(category_id) ON DELETE CASCADE,
  name TEXT NOT NULL COLLATE NOCASE,
  content TEXT NOT NULL,
  PRIMARY KEY(category_id, name)
) WITHOUT ROWID""")
      stmt.execute(
        """CREATE TABLE IF NOT EXISTS message_log (
  event_id INTEGER NOT NULL PRIMARY KEY,
  guild_id INTEGER NOT NULL,
  channel_id INTEGER NOT NULL,
  message_id INTEGER NOT NULL,
  event_type INTEGER NOT NULL,
  timestamp_secs INTEGER NOT NULL,
  content TEXT
)""")
      stmt.execute(
        s"""CREATE INDEX IF NOT EXISTS message_creation
ON message_log(guild_id, channel_id, message_id) WHERE event_type=${MessageLog.EventType.Create.id}""")
      stmt.execute("CREATE INDEX IF NOT EXISTS message_ids ON message_log(guild_id, message_id)")
      stmt.execute(
        """CREATE TABLE IF NOT EXISTS disabled_modules (
  guild_id INTEGER NOT NULL,
  module_name TEXT NOT NULL,
  PRIMARY KEY(guild_id, module_name)
) WITHOUT ROWID""")
    }
  }
}
