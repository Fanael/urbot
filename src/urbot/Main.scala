// Copyright (C) 2018 Fanael Linithien
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along
// with this program. If not, see <https://www.gnu.org/licenses/>.
package urbot

import java.io.BufferedReader
import java.io.FileReader

import ch.qos.logback.classic.LoggerContext
import ch.qos.logback.classic.joran.JoranConfigurator
import ch.qos.logback.core.joran.spi.JoranException
import ch.qos.logback.core.util.StatusPrinter
import javax.security.auth.login.LoginException
import net.dv8tion.jda.core.AccountType
import net.dv8tion.jda.core.JDABuilder
import org.json.JSONArray
import org.json.JSONObject
import org.json.JSONTokener
import org.slf4j.LoggerFactory

object Main {
  def main(args: Array[String]): Unit = {
    setupLogger()
    val instances = loadInstances()
    val databaseConnection = new DatabaseConnection(DATABASE_PATH, instances.length)
    val builder = new JDABuilder(AccountType.BOT).setAudioEnabled(false).setBulkDeleteSplittingEnabled(false)
    try {
      instances.foreach { instance =>
        val listener = new EventHandler(new Bot(databaseConnection, instance.prefix))
        builder.setToken(instance.token).addEventListener(listener).build()
        builder.removeEventListener(listener)
      }
    } catch {
      case e: LoginException => System.err.println("Login failed: " + e)
    }
  }

  private def loadInstances(): Vector[Instance] = {
    Utils.withResource(new FileReader(INSTANCE_CONFIG_PATH)) { fileReader =>
      val bufferedReader = new BufferedReader(fileReader)
      val tokener = new JSONTokener(bufferedReader)
      var result = Vector[Instance]()
      new JSONArray(tokener).forEach { obj =>
        val jsonObject = obj.asInstanceOf[JSONObject]
        val token = jsonObject.get("token").asInstanceOf[String]
        val prefix = jsonObject.get("prefix").asInstanceOf[String]
        result = result :+ Instance(token, prefix)
      }
      result
    }
  }

  private def setupLogger(): Unit = {
    val context = LoggerFactory.getILoggerFactory.asInstanceOf[LoggerContext]

    try {
      val configurator = new JoranConfigurator
      configurator.setContext(context)
      context.reset()
      configurator.doConfigure(LOG_CONFIG_PATH)
    } catch {
      case _: JoranException => // StatusPrinter will handle this.
    }
    StatusPrinter.printInCaseOfErrorsOrWarnings(context)
  }

  private val DATABASE_PATH = "db.sqlite3"
  private val LOG_CONFIG_PATH = "conf/logback.xml"
  private val INSTANCE_CONFIG_PATH = "conf/instances.json"

  private final case class Instance(token: String, prefix: String)

}
